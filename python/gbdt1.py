from __future__ import division
import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
from datetime import date
from dateutil.relativedelta import relativedelta
from sklearn import preprocessing, ensemble


pd.set_option('display.max_columns', 80)
pd.set_option('display.max_rows', 200)

mapping_dict = {
    'ind_empleado': {'N': 0, -99: 1, 'B': 2, 'F': 3, 'A': 4, 'S': 5},
    'sexo': {'V': 0, 'H': 1, -99: 2},
    'ind_nuevo': {0.0: 0, 1.0: 1, -99.0: 2},
    'indrel': {1.0: 0, 99.0: 1, -99.0: 2},
    'indrel_1mes': {-99: 0, 1.0: 1, 1: 1, 2.0: 2, 2: 2, 3.0: 3, 3: 3, 4.0: 4, 4: 4, 'P': 5},
    'tiprel_1mes': {-99: 0, 'I': 1, 'A': 2, 'P': 3, 'R': 4, 'N': 5},
    'indresi': {-99: 0, 'S': 1, 'N': 2},
    'indext': {-99: 0, 'S': 1, 'N': 2},
    'conyuemp': {-99: 0, 'S': 1, 'N': 2},
    'indfall': {-99: 0, 'S': 1, 'N': 2},
    'tipodom': {-99.0: 0, 1.0: 1},
    'ind_actividad_cliente': {0.0: 0, 1.0: 1, -99.0: 2},
    'segmento': {'02 - PARTICULARES': 0, '03 - UNIVERSITARIO': 1, '01 - TOP': 2, -99: 2},
    'pais_residencia': {'LV': 102, 'BE': 12, 'BG': 50, 'BA': 61, 'BM': 117, 'BO': 62, 'JP': 82, 'JM': 116, 'BR': 17,
                        'BY': 64, 'BZ': 113, 'RU': 43, 'RS': 89, 'RO': 41, 'GW': 99, 'GT': 44, 'GR': 39, 'GQ': 73,
                        'GE': 78, 'GB': 9, 'GA': 45, 'GN': 98, 'GM': 110, 'GI': 96, 'GH': 88, 'OM': 100, 'HR': 67,
                        'HU': 106, 'HK': 34, 'HN': 22, 'AD': 35, 'PR': 40, 'PT': 26, 'PY': 51, 'PA': 60, 'PE': 20,
                        'PK': 84, 'PH': 91, 'PL': 30, 'EE': 52, 'EG': 74, 'ZA': 75, 'EC': 19, 'AL': 25, 'VN': 90,
                        'ET': 54, 'ZW': 114, 'ES': 0, 'MD': 68, 'UY': 77, 'MM': 94, 'ML': 104, 'US': 15, 'MT': 118,
                        'MR': 48, 'UA': 49, 'MX': 16, 'IL': 42, 'FR': 8, 'MA': 38, 'FI': 23, 'NI': 33, 'NL': 7,
                        'NO': 46, 'NG': 83, 'NZ': 93, 'CI': 57, 'CH': 3, 'CO': 21, 'CN': 28, 'CM': 55, 'CL': 4, 'CA': 2,
                        'CG': 101, 'CF': 109, 'CD': 112, 'CZ': 36, 'CR': 32, 'CU': 72, 'KE': 65, 'KH': 95, 'SV': 53,
                        'SK': 69, 'KR': 87, 'KW': 92, 'SN': 47, 'SL': 97, 'KZ': 111, 'SA': 56, 'SG': 66, 'SE': 24,
                        'DO': 11, 'DJ': 115, 'DK': 76, 'DE': 10, 'DZ': 80, 'MK': 105, -99: 1, 'LB': 81, 'TW': 29,
                        'TR': 70, 'TN': 85, 'LT': 103, 'LU': 59, 'TH': 79, 'TG': 86, 'LY': 108, 'AE': 37, 'VE': 14,
                        'IS': 107, 'IT': 18, 'AO': 71, 'AR': 13, 'AU': 63, 'AT': 6, 'IN': 31, 'IE': 5, 'QA': 58,
                        'MZ': 27},
    'canal_entrada': {'013': 49, 'KHP': 160, 'KHQ': 157, 'KHR': 161, 'KHS': 162, 'KHK': 10, 'KHL': 0, 'KHM': 12,
                      'KHN': 21, 'KHO': 13, 'KHA': 22, 'KHC': 9, 'KHD': 2, 'KHE': 1, 'KHF': 19, '025': 159, 'KAC': 57,
                      'KAB': 28, 'KAA': 39, 'KAG': 26, 'KAF': 23, 'KAE': 30, 'KAD': 16, 'KAK': 51, 'KAJ': 41, 'KAI': 35,
                      'KAH': 31, 'KAO': 94, 'KAN': 110, 'KAM': 107, 'KAL': 74, 'KAS': 70, 'KAR': 32, 'KAQ': 37,
                      'KAP': 46, 'KAW': 76, 'KAV': 139, 'KAU': 142, 'KAT': 5, 'KAZ': 7, 'KAY': 54, 'KBJ': 133,
                      'KBH': 90, 'KBN': 122, 'KBO': 64, 'KBL': 88, 'KBM': 135, 'KBB': 131, 'KBF': 102, 'KBG': 17,
                      'KBD': 109, 'KBE': 119, 'KBZ': 67, 'KBX': 116, 'KBY': 111, 'KBR': 101, 'KBS': 118, 'KBP': 121,
                      'KBQ': 62, 'KBV': 100, 'KBW': 114, 'KBU': 55, 'KCE': 86, 'KCD': 85, 'KCG': 59, 'KCF': 105,
                      'KCA': 73, 'KCC': 29, 'KCB': 78, 'KCM': 82, 'KCL': 53, 'KCO': 104, 'KCN': 81, 'KCI': 65,
                      'KCH': 84, 'KCK': 52, 'KCJ': 156, 'KCU': 115, 'KCT': 112, 'KCV': 106, 'KCQ': 154, 'KCP': 129,
                      'KCS': 77, 'KCR': 153, 'KCX': 120, 'RED': 8, 'KDL': 158, 'KDM': 130, 'KDN': 151, 'KDO': 60,
                      'KDH': 14, 'KDI': 150, 'KDD': 113, 'KDE': 47, 'KDF': 127, 'KDG': 126, 'KDA': 63, 'KDB': 117,
                      'KDC': 75, 'KDX': 69, 'KDY': 61, 'KDZ': 99, 'KDT': 58, 'KDU': 79, 'KDV': 91, 'KDW': 132,
                      'KDP': 103, 'KDQ': 80, 'KDR': 56, 'KDS': 124, 'K00': 50, 'KEO': 96, 'KEN': 137, 'KEM': 155,
                      'KEL': 125, 'KEK': 145, 'KEJ': 95, 'KEI': 97, 'KEH': 15, 'KEG': 136, 'KEF': 128, 'KEE': 152,
                      'KED': 143, 'KEC': 66, 'KEB': 123, 'KEA': 89, 'KEZ': 108, 'KEY': 93, 'KEW': 98, 'KEV': 87,
                      'KEU': 72, 'KES': 68, 'KEQ': 138, -99: 6, 'KFV': 48, 'KFT': 92, 'KFU': 36, 'KFR': 144, 'KFS': 38,
                      'KFP': 40, 'KFF': 45, 'KFG': 27, 'KFD': 25, 'KFE': 148, 'KFB': 146, 'KFC': 4, 'KFA': 3, 'KFN': 42,
                      'KFL': 34, 'KFM': 141, 'KFJ': 33, 'KFK': 20, 'KFH': 140, 'KFI': 134, '007': 71, '004': 83,
                      'KGU': 149, 'KGW': 147, 'KGV': 43, 'KGY': 44, 'KGX': 24, 'KGC': 18, 'KGN': 11},
}

# feature_cols = ["ind_empleado", "pais_residencia", "sexo", "age", "ind_nuevo", "antiguedad", "nomprov", "segmento"]
conti_cols = ['age', 'ind_nuevo', 'antiguedad', 'renta']
target_cols = ['ind_ahor_fin_ult1', 'ind_aval_fin_ult1', 'ind_cco_fin_ult1', 'ind_cder_fin_ult1', 'ind_cno_fin_ult1',
               'ind_ctju_fin_ult1', 'ind_ctma_fin_ult1', 'ind_ctop_fin_ult1', 'ind_ctpp_fin_ult1', 'ind_deco_fin_ult1',
               'ind_deme_fin_ult1', 'ind_dela_fin_ult1', 'ind_ecue_fin_ult1', 'ind_fond_fin_ult1', 'ind_hip_fin_ult1',
               'ind_plan_fin_ult1', 'ind_pres_fin_ult1', 'ind_reca_fin_ult1', 'ind_tjcr_fin_ult1', 'ind_valo_fin_ult1',
               'ind_viv_fin_ult1', 'ind_nomina_ult1', 'ind_nom_pens_ult1', 'ind_recibo_ult1']


# target_cols = np.array(target_cols)

def generate_hist(train, y, m, d, month):
    train = train[['ncodpers', 'fecha_dato'] + target_cols]
    end = date(y, m, d)
    start = date(y, m, d) + relativedelta(months=month)
    train_hist = train[(train.fecha_dato >= str(start)) & (train.fecha_dato < str(end))].groupby('ncodpers')[
        target_cols].sum()
    for col in target_cols:
        train_hist[col] = train_hist[col] / abs(month)
    train_hist = train_hist.add_prefix(str(abs(month)))
    train_hist = train_hist.reset_index()
    return train_hist


def generate_simple_y(df):
    ydf = df[['ncodpers'] + target_cols]
    ydf = ydf.sort_values(['ncodpers'])
    return ydf


def simon_apk(actual, predicted, k=7):
    if len(predicted) > k:
        predicted = predicted[:k]
    score = 0.0
    num_hits = 0.0
    for i, p in enumerate(predicted):
        if actual[p] == 1:
            num_hits += 1.0
            score += num_hits / (i + 1.0)
    if score < 1.0:
        return 0.0
    return score / k

def isnumber(aString):
    try:
        float(aString)
        return True
    except:
        return False

data_source = '/Users/simon/kaggle/kaggle-sp-rec/sprec_data/'
train = pd.read_csv(data_source + 'train_ver2.csv')
test = pd.read_csv(data_source + 'test_ver2.csv')
cols_to_use = list(mapping_dict.keys())
onehotall = train[cols_to_use].append(test[cols_to_use])

for col in cols_to_use:
    map = mapping_dict[col]
    onehotall[col].fillna(-100, inplace=True)
    onehotall[col] = onehotall[col].apply(lambda x: map[x] if map.get(x, False) else x)
onehotall = pd.get_dummies(onehotall[cols_to_use])
train_onehot = onehotall[:train.shape[0]]
test_onehot = onehotall[train.shape[0]:]
train_conti = train[['ncodpers', 'fecha_dato'] + conti_cols]
for col in conti_cols:
    train_conti[col].fillna(-99, inplace=True)
test_conti = test[['ncodpers', 'fecha_dato'] + conti_cols]
for col in conti_cols:
    test_conti[col].fillna(-99, inplace=True)

del onehotall
train_tagcol = train[target_cols]
train_oh_conti = pd.concat([train_conti, train_onehot, train_tagcol], axis=1)
del train_conti, train_onehot, train_tagcol
test_oh_conti = pd.concat([test_conti, test_onehot], axis=1)
del test_conti, test_onehot
del train, test

tr = train_oh_conti[train_oh_conti.fecha_dato == '2016-04-28']
val = train_oh_conti[train_oh_conti.fecha_dato == '2016-05-28']

tr_hist6 = generate_hist(train_oh_conti, 2016, 4, 28, -6)
# tr_hist12 = generate_hist(train_oh_conti,2016,4,28,-12)
train_set = pd.merge(tr.ix[:,:-24], tr_hist6, on=['ncodpers'], how='left')
# train_set = pd.merge(train_set,tr_hist12,on=['ncodpers'],how='left')
train_set = train_set.sort_values(['ncodpers'])
trainy = generate_simple_y(tr)
trainnpy = np.array(trainy.fillna(0)).astype('int')[:, 1:]
trainnpx = np.array(train_set.fillna(-99))[:, 2:]
model = ensemble.RandomForestClassifier(n_estimators=10, max_depth=10, min_samples_leaf=10, n_jobs=4, random_state=2016)
model.fit(trainnpx, trainnpy)
del trainnpx,trainnpy,train_set,trainy,tr_hist6
#
# val_hist6 = generate_hist(train_oh_conti, 2016, 5, 28, -6)
# # val_hist12 = generate_hist(train_oh_conti,2016,5,28,-12)
# val_set = pd.merge(val[val.ix[:,-24]], val_hist6, on=['ncodpers'], how='left')
# # val_set = pd.merge(val_set,val_hist12,on=['ncodpers'],how='left')
# val_set = val_set.sort_values(['ncodpers'])
# valy = generate_simple_y(val)
# valnpy = np.array(valy.fillna(0)).astype('int')[:, 1:]
# valnpx = np.array(val_set.fillna(-99))[:, 2:]

test_hist6 = generate_hist(train_oh_conti,2016,6,28,-6)
test_oh_conti['renta'] = test_oh_conti['renta'].apply(lambda x:x if isnumber(x) else -99)
test_oh_conti['renta'] = test_oh_conti['renta'].astype('float')
test_set = pd.merge(test_oh_conti,test_hist6,on=['ncodpers'],how='left')
test_set = test_set.sort_values(['ncodpers'])
testnpx = np.array(test_set)[:,2:]


#
# preds = np.array(model.predict_proba(valnpx))[:, :, 1].T
# preds = np.argsort(preds, axis=1)
# preds = np.fliplr(preds)
# val_hist_y = tr[['ncodpers'] + target_cols]
# val_hist_y = pd.merge(valy, val_hist_y, on=['ncodpers'], how='left')
# val_hist_y = val_hist_y.fillna(0)
# hist_flag = 24
# total_score = 0
# for idx, row in val_hist_y.ix[:, 1:].iterrows():
#     score = 0.0
#     num_hits = 0.0
#     i = 0
#     for pred in preds[idx]:
#         if i>=7:
#             break
#         if row[hist_flag + pred] == 1:
#             continue
#         else:
#             if row[pred] == 1:
#                 num_hits += 1.0
#                 score += num_hits / (i + 1.0)
#             i += 1
#     score = score / i
#     print score
#     total_score += score

test_preds = np.array(model.predict_proba(testnpx))[:, :, 1].T
test_preds = np.argsort(test_preds, axis=1)
test_preds = np.fliplr(test_preds)
test_hist_y = val[['ncodpers'] + target_cols]
test_hist_y = pd.merge(test, test_hist_y, on=['ncodpers'], how='left')
test_hist_y = test_hist_y.fillna(0)
total_score = 0
test_hist_flag = 25
final_preds = []
for idx, row in test_hist_y.iterrows():
    new_top_products = []
    i= 0
    for pred in test_preds[idx]:
        if i>=7:
            break
        if row[test_hist_flag + pred] == 1:
            continue
        else:
            new_top_products.append(target_cols[pred])
            i+=1
    final_preds.append(" ".join(new_top_products))
out_df = pd.DataFrame({'ncodpers': test_hist_y['ncodpers'], 'added_products': final_preds})
out_df.to_csv('sub_rf.csv', index=False)
