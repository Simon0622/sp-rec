import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from sklearn.metrics import roc_auc_score
from sklearn.linear_model import LogisticRegression
from collections import defaultdict
pd.set_option('display.max_columns', 80)
pd.set_option('display.max_rows', 200)

# def apk(actual, predicted, k=7):
#     if len(predicted) > k:
#         predicted = predicted[:k]

#     score = 0.0
#     num_hits = 0.0

#     for i, p in enumerate(predicted):
#         if p in actual and p not in predicted[:i]:
#             num_hits += 1.0
#             score += num_hits / (i+1.0)

#     if not actual:
#         return 0.0

#     return score / min(len(actual), k)

datasource = './'
probdf = pd.read_csv(datasource+'predictv2.txt',header=None,names=['prob'],sep='\s')
ncode = pd.read_csv(datasource+'test_ffm_hash.txt.dic',header=None,names=['ncode','product'],sep='\s')
df = pd.concat([ncode,probdf],axis=1)
df = df.sort_values(by=['ncode','prob'],ascending=False)
df = df.reset_index()
del df['index']
df['rank'] = df.index
df['rank'] = df['rank'].apply(lambda x:x%24)
table = pd.pivot_table(df,values=['product'],index=['ncode'],columns=['rank'],aggfunc='first')
submission = table[list(range(7))]
sub1 = submission['product']
sub1 = sub1.reset_index()
sub2 = pd.DataFrame(columns=['ncodpers','0','1','2','3','4','5','6'],data=sub1.values)
sub2['all'] = sub2.apply(lambda x:x['0']+' '+x['1']+' '+x['2']+' '+x['3']+' '+x['4']+' '+x['5']+' '+x['6'],axis=1)
sub = sub2[['ncodpers','all']]
sub1 = pd.DataFrame(columns=['ncodpers','added_products'],data=sub.values)
sub1.to_csv(datasource+'submission.csv',index=False)