import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from sklearn.metrics import roc_auc_score
from sklearn.linear_model import LogisticRegression
from collections import defaultdict
from time import time
pd.set_option('display.max_columns', 80)
pd.set_option('display.max_rows', 200)

datasource = './'
onehot_cols = ['ind_empleado','pais_residencia','sexo','ind_nuevo',
'indrel','ult_fec_cli_1t','indrel_1mes','tiprel_1mes','indresi','indext','conyuemp','canal_entrada',
'indfall','tipodom','nomprov','ind_actividad_cliente','segmento']
conti_cols = ['age','antiguedad','cod_prov','renta']
target_cols = []
old_cols = []
maps = {}
count = 0

def apk(actual, predicted, k=7):
    if len(predicted) > k:
        predicted = predicted[:k]

    score = 0.0
    num_hits = 0.0

    for i, p in enumerate(predicted):
        if p in actual and p not in predicted[:i]:
            num_hits += 1.0
            score += num_hits / (i+1.0)

    if not actual:
        return 0.0

    return score / min(len(actual), k)

def generate_onehot(df,onehot_cols):
	global count
	global maps
	for col in onehot_cols:
		value_list = df[col].unique()
		for value in value_list:
			key = col+'_'+str(value)
			count = count+1
			maps[key] = count

def generate_continuous(df,conti_cols):
	global count
	global maps
	for col in conti_cols:
		count = count+1
		maps[col] = count


def generate_featmap(df):
	generate_onehot(df,onehot_cols)
	generate_continuous(df,conti_cols)
	generate_continuous(df,target_cols)


def print_map(lists,filename):
	fo = open(filename, 'w')
	for item in lists:
		s =  item[0]+':'+str(item[1])
		print s
		fo.write(s+'\n')

def get_hist_feat(traindf,histdf):
	global target_cols
	old_set_gb = histdf.groupby('ncodpers')[target_cols].sum()
	old_set_gb = old_set_gb.add_prefix('old_')
	old_set_gb = old_set_gb.reset_index()
	train_set = pd.merge(traindf,old_set_gb,on=['ncodpers'],how='left')
	return train_set


def generate_liblinear_train(traindf):
	global maps
	global count
	global old_cols
	####onehot feature#####
	generate_featmap(traindf)
	
	val_set = traindf[traindf.fecha_dato=='2016-05-28']
	val_hist_set = traindf[(traindf.fecha_dato>'2015-01-28') & (traindf.fecha_dato<'2016-05-28')]
	train_set = traindf[traindf.fecha_dato=='2016-04-28']
	train_hist_set = traindf[traindf.fecha_dato<'2016-04-28']
	train_set = get_hist_feat(train_set,train_hist_set)
	val_set = get_hist_feat(val_set,val_hist_set)

	old_cols = train_set.columns[48:]
	for col in old_cols:
		for k in range(1,17):
			key = col+'_'+str(k)
			count = count+1
			maps[key] = count

	for i in target_cols:
		for j in old_cols:
			for k in range(1,17):
				key = i+'_'+j+'_'+str(k)
				count = count+1
				maps[key] = count
	
	maplist = sorted(maps.items(), lambda x, y: cmp(x[1], y[1]))
	print_map(maplist,datasource+'featmap')
	generate_liblinear_file(train_set,datasource+'train_linear.txt')
	generate_liblinear_file(val_set,datasource+'val_linear.txt')
	####onehot feature#####

def generate_liblinear_file(linear_df,filename):
	fo = open(filename, 'w')
	contimaxmap = {}
	contiminmap = {}
	for col in conti_cols:
		maxcol = linear_df[col].max()
		mincol = linear_df[col].min()
		contimaxmap[col] = maxcol
		contiminmap[col] = mincol
		print col+' '+maxcol+' '+mincol
	for index,row in linear_df.iterrows():
		strs = ''
		for col in onehot_cols:
			value = str(row[col])
			if maps.get(col+'_'+value,False):
				strs = strs+' '+str(maps.get(col+'_'+value))+':1'

		for col in conti_cols:
			if pd.notnull(row[col]):
				value = (row[col]-contiminmap[col])/(contimaxmap[col]-contiminmap[col])
				if maps.get(col,False):
					strs = strs+' '+str(maps.get(col))+':'+str(value)
		for tcol in target_cols:
			s = strs
			label = ''
			if row[tcol]==0 or pd.isnull(row[tcol]):
				label = '-1 '
			else:
				label = '+1 '
			s = label+s
			s = s+' '+str(maps.get(tcol))+':1'

			for col in old_cols:
				if pd.isnull(row[col]):
					row[col] = 0
				value = int(row[col])
				if value > 0:
					value = str(value)
					if maps.get(col+'_'+value,False):
						s = s+' '+str(maps.get(col+'_'+value))+':1'
					if maps.get(tcol+'_'+col+'_'+value,False):
						s = s+' '+str(maps.get(tcol+'_'+col+'_'+value))+':1'
			fo.write(s + '\n')
	

def generate_ffm_train(traindf):
	global maps
	global count
	global old_cols
	####onehot feature#####
	generate_featmap(traindf)
	
	val_set = traindf[traindf.fecha_dato=='2016-05-28']
	val_hist_set = traindf[traindf.fecha_dato>'2015-01-28']
	train_set = traindf[traindf.fecha_dato=='2016-04-28']
	train_hist_set = traindf[traindf.fecha_dato<'2016-04-28']
	train_set = get_hist_feat(train_set,train_hist_set)
	val_set = get_hist_feat(val_set,val_hist_set)

	old_cols = train_set.columns[48:]
	for col in old_cols:
		for k in range(1,17):
			key = col+'_'+str(k)
			count = count+1
			maps[key] = count

	for i in target_cols:
		for j in old_cols:
			for k in range(1,17):
				key = i+'_'+j+'_'+str(k)
				count = count+1
				maps[key] = count
	
	maplist = sorted(maps.items(), lambda x, y: cmp(x[1], y[1]))
	print_map(maplist,datasource+'featmap')
	generate_ffm_file(train_set,datasource+'train_ffm.txt')
	generate_ffm_file(val_set,datasource+'val_ffm.txt')

def isNum(value):
    try:
        value + 1
    except TypeError:
        return False
    else:
        return True

def generate_ffm_file(ffm_df,filename):
	fo = open(filename, 'w')
	contimaxmap = {}
	contiminmap = {}
	for col in conti_cols:
		maxcol = ffm_df[col].max()
		mincol = ffm_df[col].min()
		contimaxmap[col] = maxcol
		contiminmap[col] = mincol
		#print col+' '+str(maxcol)+' '+str(mincol)
	for index,row in ffm_df.iterrows():
		strs = ''
		for col in onehot_cols:
			value = str(row[col])
			if maps.get(col+'_'+value,False):
				strs = strs+' 1:'+str(maps.get(col+'_'+value))+':1'

		for col in conti_cols:
			if pd.notnull(row[col]) and isNum(row[col]):
				value = float(row[col])
				value = (value-contiminmap[col])/(contimaxmap[col]-contiminmap[col])
				if maps.get(col,False):
					strs = strs+' 1:'+str(maps.get(col))+':'+str(value)
		
		for tcol in target_cols:
			s = strs
			label = ''
			if row[tcol]==0 or pd.isnull(row[tcol]):
				label = '0 '
			else:
				label = '1 '
			s = label+s
			s = s+' 2:'+str(maps.get(tcol))+':1'

			for col in old_cols:
				if pd.isnull(row[col]):
					row[col] = 0
				value = int(row[col])
				if value > 0:
					value = str(value)
					if maps.get(col+'_'+value,False):
						s = s+' 3:'+str(maps.get(col+'_'+value))+':1'
					if maps.get(tcol+'_'+col+'_'+value,False):
						s = s+' 4:'+str(maps.get(tcol+'_'+col+'_'+value))+':1'
			fo.write(s + '\n')


def ELF_hash(code,hashsize=1000000):
	length = len(str(hashsize))
	hashcode = 0
	x = 0
	strcode = str(code)
	for i, ch in enumerate(strcode):
		hashcode = (hashcode << 4) + ord(ch)
		x = hashcode & 0xF0000000
		if x != 0:
			hashcode ^= (x >> 24)
		hashcode &= ~x
	result = (hashcode & 0x7FFFFFFF) % hashsize
	return str(result).zfill(length)

hashmap = {}

def generate_liblinear_wrapper(traindf,filename,isval=False):
	print traindf.shape
	global target_cols
	global old_cols
	generate_liblinear_byhash(traindf,filename,onehot_cols,conti_cols,target_cols,old_cols,isval)
def generate_liblinear_test_wrapper(traindf,filename,isval=True):
	print traindf.shape
	global target_cols
	global old_cols
	generate_test_liblinear_byhash(traindf,filename,onehot_cols,conti_cols,target_cols,old_cols,isval)

def generate_libffm_wrapper(traindf,filename,isval=False):
	global target_cols
	global old_cols
	generate_ffm_byhash(traindf,filename,onehot_cols,conti_cols,target_cols,old_cols,isval)
def generate_libffm_test_wrapper(traindf,filename,isval=True):
	global target_cols
	global old_cols
	generate_test_ffm_byhash(traindf,filename,onehot_cols,conti_cols,target_cols,old_cols,isval)

def generate_liblinear_byhash(traindf,filename,onehot_cols,conti_cols,target_cols,old_cols,isval=False):
	print target_cols,old_cols
	fo = open(filename, 'w')
	bakfile = open(filename+'.dic', 'w')
	# contimaxmap = {}
	# contiminmap = {}
	# for col in conti_cols:
	# 	maxcol = traindf[col].max()
	# 	mincol = traindf[col].min()
	# 	contimaxmap[col] = maxcol
	# 	contiminmap[col] = mincol
	# 	print col+' '+str(maxcol)+' '+str(mincol)
	for index,row in traindf.iterrows():
		rowstr = ''
		k = 1
		# for i in range(len(onehot_cols)):
		# 	hashcode = str(k+i)+ELF_hash(onehot_cols[i]+'_'+str(row[onehot_cols[i]]),10000)
		# 	rowstr = rowstr+' '+hashcode+':1'
		# k = len(onehot_cols)
		# for i in range(len(conti_cols)):
		# 	if pd.notnull(row[conti_cols[i]]) and isNum(row[conti_cols[i]]):
		# 		hashcode = str(k+i+1)+ELF_hash(conti_cols[i],10000)
		# 		value = float(row[conti_cols[i]])
		# 		value = (value-contiminmap[conti_cols[i]])/(contimaxmap[conti_cols[i]]-contiminmap[conti_cols[i]])
		# 		rowstr = rowstr+' '+hashcode+':'+str(value)
		# k = k+len(conti_cols)+1

		for tcol in target_cols:
			if isval:
				bakfile.write(str(row['ncodpers'])+' '+tcol + '\n')
			s = rowstr
			label = ''
			if row[tcol]==0 or pd.isnull(row[tcol]):
				label = '-1 '
			else:
				label = '+1 '
			s = label+s
			s = s+' '+str(k)+ELF_hash(tcol,10000)+':1'

			i = 1
			for col in old_cols:
				i = i+2
				if pd.isnull(row[col]):
					row[col] = 0
				value = int(row[col])
				if value > 0:
					value = str(value)
					s = s+' '+str(k+i)+ELF_hash(col,10000)+':1'
					s = s+' '+str(k+i+1)+ELF_hash(tcol+'_'+col,10000)+':1'
			
			fo.write(s + '\n')

	fo.close()
	bakfile.close()

def generate_test_liblinear_byhash(traindf,filename,onehot_cols,conti_cols,target_cols,old_cols,isval=True):
	print target_cols,old_cols
	fo = open(filename, 'w')
	bakfile = open(filename+'.dic', 'w')
	# contimaxmap = {}
	# contiminmap = {}
	# for col in conti_cols:
	# 	maxcol = traindf[col].max()
	# 	mincol = traindf[col].min()
	# 	contimaxmap[col] = maxcol
	# 	contiminmap[col] = mincol
	# 	print col+' '+str(maxcol)+' '+str(mincol)

	for index,row in traindf.iterrows():
		rowstr = ''
		k = 1
		# for i in range(len(onehot_cols)):
		# 	hashcode = str(k+i)+ELF_hash(onehot_cols[i]+'_'+str(row[onehot_cols[i]]),10000)
		# 	rowstr = rowstr+' '+hashcode+':1'
		# k = len(onehot_cols)
		# for i in range(len(conti_cols)):
		# 	if pd.notnull(row[conti_cols[i]]) and isNum(row[conti_cols[i]]):
		# 		hashcode = str(k+i+1)+ELF_hash(conti_cols[i],10000)
		# 		value = float(row[conti_cols[i]])
		# 		value = (value-contiminmap[conti_cols[i]])/(contimaxmap[conti_cols[i]]-contiminmap[conti_cols[i]])
		# 		rowstr = rowstr+' '+hashcode+':'+str(value)
		# k = k+len(conti_cols)+1

		for tcol in target_cols:
			if isval:
				bakfile.write(str(row['ncodpers'])+' '+tcol + '\n')
			s = rowstr
			label = '-1'
			s = label+s
			s = s+' '+str(k)+ELF_hash(tcol,10000)+':1'

			i = 1
			for col in old_cols:
				i = i+2
				if pd.isnull(row[col]):
					row[col] = 0
				value = int(row[col])
				if value > 0:
					value = str(value)
					s = s+' '+str(k+i)+ELF_hash(col,10000)+':1'
					s = s+' '+str(k+i+1)+ELF_hash(tcol+'_'+col,10000)+':1'

			fo.write(s + '\n')
	fo.close()
	bakfile.close()

def generate_ffm_byhash(traindf,filename,onehot_cols,conti_cols,target_cols,old_cols,isval=False):
	print target_cols,old_cols
	fo = open(filename, 'w')
	bakfile = open(filename+'.dic', 'w')
	contimaxmap = {}
	contiminmap = {}
	for col in conti_cols:
		maxcol = traindf[col].max()
		mincol = traindf[col].min()
		contimaxmap[col] = maxcol
		contiminmap[col] = mincol
		print col+' '+str(maxcol)+' '+str(mincol)

	for index,row in traindf.iterrows():
		rowstr = ''
		for i in range(len(onehot_cols)):
			hashcode = ELF_hash(onehot_cols[i]+'_'+str(row[onehot_cols[i]]))
			hashcode = str(hashcode)
			rowstr = rowstr+' 1:'+hashcode+':1'
		k = len(onehot_cols)
		for i in range(len(conti_cols)):
			if pd.notnull(row[conti_cols[i]]) and isNum(row[conti_cols[i]]):
				hashcode = ELF_hash(conti_cols[i])
				value = float(row[conti_cols[i]])
				value = (value-contiminmap[conti_cols[i]])/(contimaxmap[conti_cols[i]]-contiminmap[conti_cols[i]])
				rowstr = rowstr+' 1:'+hashcode+':'+str(value)
		k = k+len(conti_cols)+1

		for tcol in target_cols:
			if isval:
				bakfile.write(str(row['ncodpers'])+' '+tcol + '\n')
			s = rowstr
			label = ''
			if row[tcol]==0 or pd.isnull(row[tcol]):
				label = '0'
			else:
				label = '1'
			s = label+s
			s = s+' 2:'+ELF_hash(tcol)+':1'

			i = 1
			for col in old_cols:
				i = i+2
				if pd.isnull(row[col]):
					row[col] = 0
				value = int(row[col])
				if value > 0:
					value = str(value)
					s = s+' 3:'+ELF_hash(col+'_'+value)+':1'
					s = s+' 4:'+ELF_hash(tcol+'_'+col+'_'+value)+':1'
			fo.write(s + '\n')
	fo.close()
	bakfile.close()

def generate_test_ffm_byhash(traindf,filename,onehot_cols,conti_cols,target_cols,old_cols,isval=True):
	print target_cols,old_cols
	fo = open(filename, 'w')
	bakfile = open(filename+'.dic', 'w')
	contimaxmap = {}
	contiminmap = {}
	for col in conti_cols:
		maxcol = traindf[col].max()
		mincol = traindf[col].min()
		contimaxmap[col] = maxcol
		contiminmap[col] = mincol
		print col+' '+str(maxcol)+' '+str(mincol)

	for index,row in traindf.iterrows():
		rowstr = ''
		for i in range(len(onehot_cols)):
			hashcode = ELF_hash(onehot_cols[i]+'_'+str(row[onehot_cols[i]]))
			#m = "%03d" % i
			hashcode = str(hashcode)
			rowstr = rowstr+' 1:'+hashcode+':1'
		k = len(onehot_cols)
		for i in range(len(conti_cols)):
			if pd.notnull(row[conti_cols[i]]) and isNum(row[conti_cols[i]]):
				hashcode = ELF_hash(conti_cols[i])
				value = float(row[conti_cols[i]])
				value = (value-contiminmap[conti_cols[i]])/(contimaxmap[conti_cols[i]]-contiminmap[conti_cols[i]])
				rowstr = rowstr+' 1:'+hashcode+':'+str(value)
		k = k+len(conti_cols)+1

		for tcol in target_cols:
			if isval:
				bakfile.write(str(row['ncodpers'])+' '+tcol + '\n')
			s = rowstr
			label = str(index)
			s = label+s
			s = s+' 2:'+ELF_hash(+tcol)+':1'
			i = 1
			for col in old_cols:
				i = i+2
				if pd.isnull(row[col]):
					row[col] = 0
				value = int(row[col])
				if value > 0:
					value = str(value)
					s = s+' 3:'+ELF_hash(col+'_'+value)+':1'
					s = s+' 4:'+ELF_hash(tcol+'_'+col+'_'+value)+':1'
			fo.write(s + '\n')
	fo.close()
	bakfile.close()

if __name__ == '__main__':
	# train_raw = pd.read_csv(datasource+'train_ver2.csv')
	# test_raw = pd.read_csv(datasource+'test_ver2.csv')

	# val_set = train_raw[train_raw.fecha_dato=='2016-05-28']
	# train_set = train_raw[train_raw.fecha_dato=='2016-04-28']
	# val_hist_set = train_raw[(train_raw.fecha_dato>'2015-01-28') & (train_raw.fecha_dato<'2016-05-28')]
	# train_hist_set = train_raw[train_raw.fecha_dato<'2016-04-28']
	# target_cols = train_raw.columns[24:]
	# train_set = get_hist_feat(train_set,train_hist_set)
	# train_set.to_csv(datasource+'train_set.csv',index=False)
	# val_set = get_hist_feat(val_set,val_hist_set)
	# val_set.to_csv(datasource+'val_set.csv',index=False)
	# old_cols = train_set.columns[48:]
    
	# test_hist_set = train_raw[train_raw.fecha_dato>'2015-02-28']
	# test_set = get_hist_feat(test_raw,test_hist_set)
	# test_set.to_csv(datasource+'test_raw.csv',index=False)

	train_set = pd.read_csv(datasource+'train_set.csv')
	val_set = pd.read_csv(datasource+'val_set.csv')
	test_set = pd.read_csv(datasource+'test_raw.csv')
	target_cols = train_set.columns[24:48]
	old_cols = train_set.columns[48:]

	#generate_liblinear_train(train_raw)
	#generate_ffm_train(train_raw)
	#generate_liblinear_wrapper(train_set,datasource+'train_linear_hash.txt')
	# generate_liblinear_wrapper(val_set,datasource+'val_linear_hash.txt',True)
	generate_liblinear_test_wrapper(test_set,datasource+'test_linear_hash.txt',True)
    #
	#generate_libffm_wrapper(train_set,datasource+'train_ffm_hash.txt')
	#generate_libffm_wrapper(val_set,datasource+'val_ffm_hash.txt',True)
	#generate_libffm_test_wrapper(test_raw,datasource+'test_ffm_hash.txt',True)